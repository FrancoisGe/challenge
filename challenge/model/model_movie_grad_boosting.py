import pandas as pd
from challenge.model.abc_model import ABC_model

import numpy as np

from sklearn.multioutput import MultiOutputRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline



class Model_movie(ABC_model):
    def __init__(self):
        print("Creat Model_movie")
        self.build_model()

    def build_model(self):
        self.model = Pipeline([
                ('tfidf', TfidfVectorizer()),
                ('clf', MultiOutputRegressor(GradientBoostingRegressor(random_state=0))),
                 ])

    def train(self, data):
        self.load_data(data)

        self.model.fit(self.X_train, self.y_train)
        predictions = self.model.predict(self.X_test)
        Model_movie.eval_acc(predictions,self.y_test)


    def predict(self, data):
        pred = self.model.predict(data["text"])

        data["pred"] = list(pred)
        return data

    def load_data(self, data):
        print("Load data Model")

        super().load_data(data)

        self.X_train = list(self.train["text"])
        self.y_train = np.array(list(self.train["vec_lab"]))

        self.X_test = list(self.test["text"])
        self.y_test = np.array(list(self.test["vec_lab"]))

        self.X_all = list(self.all["text"])
        self.y_all = np.array(list(self.all["vec_lab"]))
    
    def eval_acc(pred,true):
        error = 0
        
        for i in range(0,len(pred)):
            errors = np.subtract(np.array(pred[i]),np.array(true[i]))
            add_error = sum(np.array([abs(e)for e in errors])) /len(errors)
            error = error + add_error

        mean_error = error/len(pred)
        print("error: ",mean_error)