import pandas as pd
from challenge.model.abc_model import ABC_model

import numpy as np

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Dense, Input, LSTM, Embedding, Dropout, Activation
from keras.layers import Bidirectional, GlobalMaxPool1D
from keras.models import Model
from keras import initializers, regularizers, constraints, optimizers, layers

from keras.metrics import top_k_categorical_accuracy


class Model_movie(ABC_model):
    def __init__(self):
        print("Creat Model_movie")
        self.maxlen = 200
        self.max_features = 20000


        self.build_model()
        

    def train(self, data):
        self.load_data(data)
        print(self.train)

        batch_size = 32
        epochs = 2

        self.model.fit(self.X_all,self.y_all, batch_size=batch_size, epochs=epochs, validation_split=0.1)
        scores = self.model.evaluate(self.X_test, self.y_test, verbose=0)

        # serialize model to JSON
        model_json = self.model.to_json()
        with open("./data/model.json", "w") as json_file:
            json_file.write(model_json)
        # serialize weights to HDF5
        self.model.save_weights("./data/model.h5")
        print("Saved model to disk")
        print(self.model.metrics_names)
        print(scores)
        
        print(self.model.metrics_names)
        print(scores)
        

    def evaluate(self, data):
        self.load_data(data)
        print(self.train)

        batch_size = 32
        epochs = 2

        self.model.fit(self.X_train,self.y_train, batch_size=batch_size, epochs=epochs, validation_split=0.1)
        scores = self.model.evaluate(self.X_test, self.y_test, verbose=0)

        
        


    def load_data(self, data):
        print("Load data Model")

        super().load_data(data)
        self.tokenizer = Tokenizer(num_words=self.max_features)
        self.tokenizer.fit_on_texts(list(self.train["text"]))

        self.train["tokenized"] = self.tokenizer.texts_to_sequences(self.train["text"])
        self.test["tokenized"] = self.tokenizer.texts_to_sequences(self.test["text"])
        self.all["tokenized"] = self.tokenizer.texts_to_sequences(self.all["text"])

        print("load_data1")
        self.X_train = pad_sequences(self.train["tokenized"], maxlen=self.maxlen)
        self.y_train = np.array(list(self.train["vec_lab"]))

        self.X_test = pad_sequences(self.test["tokenized"], maxlen=self.maxlen)
        self.y_test = np.array(list(self.test["vec_lab"]))

        self.X_all = pad_sequences(self.all["tokenized"], maxlen=self.maxlen)
        self.y_all = np.array(list(self.all["vec_lab"]))
        print("load_data2")
        
    def predict(self, data):
        return super().predict(data)

    def build_model(self):

        inp = Input(shape=(self.maxlen, )) #maxlen=200 as defined earlier

        embed_size = 128
        x = Embedding(self.max_features, embed_size)(inp)
        x = LSTM(60, return_sequences=True,name='lstm_layer')(x)
        x = GlobalMaxPool1D()(x)
        x = Dropout(0.1)(x)

        x = Dense(50, activation="relu")(x)
        x = Dropout(0.1)(x)

        x = Dense(19, activation="sigmoid")(x)

        self.model = Model(inputs=inp, outputs=x)
        self.model.compile(loss='binary_crossentropy',
                        optimizer='adam',
                        metrics=['accuracy',"top_k_categorical_accuracy"])