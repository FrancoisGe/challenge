from abc import ABC,abstractmethod
import pandas as pd

from sklearn.model_selection import train_test_split

class ABC_model(ABC):

    @abstractmethod
    def train(self, data: pd.DataFrame):
        pass

    @abstractmethod
    def predict(self, data: pd.DataFrame):
        pass

    def load_data(self, data: pd.DataFrame):
        self.train, self.test = train_test_split(data, test_size=0.33, random_state=42)
        self.all = data
