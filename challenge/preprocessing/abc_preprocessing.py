from abc import ABC,abstractmethod
import pandas as pd


class ABC_preprocessing(ABC):

    @abstractmethod
    def process(data:pd.DataFrame):
        pass
