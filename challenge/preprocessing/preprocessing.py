from challenge.preprocessing.abc_preprocessing import ABC_preprocessing
import numpy as np
import spacy 
from spacy.lang.en.stop_words import STOP_WORDS
from sklearn.utils import shuffle

try :
    nlp = spacy.load("en")
except OSError:
    print("Load SpaCy model en")
    os.system("python -m spacy download en")

class Preprocessing(ABC_preprocessing):

    def process(data):
        print("Preprocessing Begin")
        list_labels = np.array([])
        data = shuffle(data)
        data["text"] = data.apply(lambda row: Preprocessing.clean_text(row["text"]),axis=1)
        if("labels" in [key for key in data]):
            print("With labels")
            data["labels"] = data.apply(lambda row: row["labels"].split(" "),axis=1)

            all_label = []
            for labs in data["labels"]:
                all_label = all_label+labs
            list_labels = np.array(list(set(all_label)))
            for lab in list_labels:
                data[lab] = data.apply(lambda row : Preprocessing.check_label(row,lab),axis = 1 )
            
            data["vec_lab"] = [list(vec) for vec in data[list_labels].values]

        return data, list_labels

    def clean_text(text):
        doc = nlp(text)
        clean_text = " ".join([ tok.lemma_.lower() for tok in doc if tok.lemma_!="-PRON-" and tok.lemma_ not in STOP_WORDS and tok.dep_ not in ["punct"]])
        return clean_text

    def check_label(row,lab):
        if lab in row["labels"]:
            return 1
        else :
            return 0