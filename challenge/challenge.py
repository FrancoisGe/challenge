from flask import Flask, request, make_response, Response, jsonify

import os
import pandas as pd
from pandas.io.json import json_normalize

import numpy as np


from challenge.preprocessing.preprocessing import Preprocessing
from challenge.model.model_movie_grad_boosting import Model_movie

app = Flask(__name__)


cache = {}

@app.route('/hello_world')
def hello_world():
    return "Hello world!"



@app.route('/genres/train', methods=['POST'])
def train():
    print("/genres/train")

    data = request.data.decode("utf-8") 



    path_local_data = "./local_data/"
    file_train_data = "train_local.csv"
    file_train_data_process = "train_process_local.csv"


    f = open(path_local_data+file_train_data, "a")
    f.write(data)
    f.close()

    data = pd.read_csv(path_local_data+file_train_data).rename(columns={"genres":"labels", "synopsis":"text"})



    data, labels = Preprocessing.process(data)
    data.to_csv(path_local_data+file_train_data_process)

    model = Model_movie()

    model.train(data)

    cache["model"] = model
    cache["labels"] = labels

    return Response(
            status=200,
            headers={
                "description":"The model was trained successfully"
            })



@app.route('/genres/predict', methods=['POST'])
def predict():
    print('/genres/predict')

    path_local_data = "./local_data/"
    file_test_data = "test_local.csv"
 
    if(request.is_json):
        json = request.json
        data = json_normalize(json).rename(columns={"synopsis":"text"})

    else:
        data = request.data.decode("utf-8") 

        f = open(path_local_data+file_test_data, "a")
        f.write(data)
        f.close()

        data = pd.read_csv(path_local_data+file_test_data).rename(columns={"synopsis":"text"})
        data = data.dropna(axis=0)


    data, _ = Preprocessing.process(data)
    data = cache["model"].predict(data)

    data["predicted_genres"] = data.apply(lambda row : predicted_genres(cache["labels"],row["pred"]),axis=1)
         

    if(request.is_json):
        result = []
        for pred in data["predicted_genres"]:
            result.append({"predicted_genres":pred})
        return jsonify(result)

    else:
        result = data[["movie_id","predicted_genres"]]

        return Response(
                result.to_csv(index=False),
                status=200,
                headers={
                    "description":"The top 5 predicted movie genres",
                    "Content-Type":"text/csv", 

                })



def predicted_genres(genres,pred):
    """
    Return string with "The top 5 predicted movie genres" separate by a space
    """
    ind = np.argpartition(pred, -4)[-5:]
    ind = np.flip(ind)

    return " ".join(genres[ind])

